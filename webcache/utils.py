import json

import furl


def is_valid_url(url):
    return type(url) == str and len(url.strip()) > 0 and url.startswith("http")


def db_normalize_url(url_item):
    the_url, the_data = (url_item, {}) if type(url_item) is str else (url_item[0], json.loads(url_item[1]))
    try:
        lower_link_furl = furl.furl(
            the_url.lower().strip().replace("https://", "http://"))  # consider http and https as EQUAL for the key
        lower_link_furl.path.normalize()
        lower_link_furl.query.params = sorted(
            [(par, lower_link_furl.query.params[par]) for par in lower_link_furl.query.params],
            key=lambda item: item[0])
        lower_link_furl.path = "%s/" % lower_link_furl.path if not str(lower_link_furl.path).endswith(
            "/") else lower_link_furl.path
        lower_link = lower_link_furl.url

        if the_data:
            data_json = json.dumps(the_data, sort_keys=True).lower()
            lower_link = f"{lower_link}_{data_json}"

        return lower_link
    except:
        print("COULD NOT DB NORM URL %s" % the_url)
        return None
