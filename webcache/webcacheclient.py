import base64
import bz2
import json
import os
import pickle
from os.path import expanduser

import requests

from settings import WEBCACHE_ADDRESS, CACHE_ENVIRONMENT
from webcache.utils import db_normalize_url, is_valid_url


class WebCacheClient: # add constructor to set webcache location programmatically. fall back to config if no explicit location provided
    WEBCACHE_LOCATION = WEBCACHE_ADDRESS

    def __init__(self):
        expected_env_location = "%s/.labscape.env" % expanduser("~") #it's probably better to specify the webcache IP in the file rather than the env name
        if os.path.exists(expected_env_location):
            with open(expected_env_location, "r") as fi:
                content = fi.read()
                if content:
                    env_type = content.strip().lower()
                    if env_type == "dev":
                        print("using DEV-environment for cache")
                    if env_type == "docker":
                        print("using docker-environment for cache")

                    self.WEBCACHE_LOCATION = CACHE_ENVIRONMENT[env_type]

    def get_proxy_list(self, num_proxies: int = 1000) -> list:
        '''
        gets list of proxies from data service. Some of the proxies might not work, but the probability of having a
        majority of good proxies is rather high.
        :param num_proxies: maximal number of proxies returned (the higher the number of proxies, the larger the share of non-working proxies. Usually you can expect there to be around 1500 working proxies in the service at any given time)
        :return: list of proxies
        '''
        if type(num_proxies) != int:
            raise ValueError("numProxies must be an integer")

        if num_proxies < 1:
            return []
        else:
            service_url = "http://%s/proxies/%s" % (self.WEBCACHE_LOCATION, num_proxies)
            data = requests.get(service_url).json()
            if data is not None and "response" in data:
                return data["response"]
            else:
                raise ValueError("could not get proxies: %s" % data)

    def fetch_urls(self, url_list, category: str, output, method="GET", max_age_days=360) -> dict:
        '''
        uses data service to fetch a list of urls

        :param url_list: non-empty list of url's to be obtained. if data is included in a POST request -> list of tuples(url, data-json)
        :param category: name of the dataprocessor issuing request and type of request. Example: "dataprocessor_geocoder:find-latlon". Only for logging purposes
        :param output: how should the cache output be interpreted and serialised? supported are JSON and XML (BeautifulSoup object returned)
        :param method: GET/POST
        :param max_age_days: maximum age of page in cache in days. If a URL has been cached longer ago than these days, it is fetched again
        :return: dictionary where input url's are mapped to cache-result. available fields in cache-result-dict: content, size, url, format, creation_date, urlKey
        '''
        if output.lower() not in ["json", "xml"]:
            raise ValueError("output-field must be either JSON or XML")

        if method.upper() not in ["GET", "POST"]:
            raise ValueError("the web cache currently only supports GET and POST Requests")

        filteredUrlList = []
        for urlItem in url_list:
            urlTuple = (urlItem, '{}') if type(urlItem) is str else urlItem

            if is_valid_url(urlTuple[0]):
                filteredUrlList.append(urlTuple)
            else:
                print("invalid URL supplied to cache: %s. will ignore it" % urlTuple[0])

        serviceURL = "http://%s/fetch/%s/%s/%s/%s" % (self.WEBCACHE_LOCATION, max_age_days, category, output, method)
        if any('localhost' in url[0] or '127.0.0.1' in url[0] for url in filteredUrlList):
            data = {}
            for url in filteredUrlList:
                data[url[0]] = {'content': requests.get(url[0]).json()}
            return data
        else:
            data = requests.post(serviceURL, {"urls": json.dumps(filteredUrlList)}).json()

        if "error" in data:
            raise ValueError("cache could not obtain data. Error: %s" % data["error"])
        else:
            urlKeys = {}
            for pageData in data["response"]:
                target_field = "content_bz2" if "content_bz2" in pageData and pageData[
                    "content_bz2"] is not None else "content_raw_bz2"
                if target_field in pageData:
                    b64decoded = base64.b64decode(pageData[target_field][2:])
                    decompressed = bz2.decompress(b64decoded)
                    if target_field != "content_bz2":
                        print("we could not parse url %s into %s" % (db_normalize_url(pageData["urlTuple"]), output))
                        pageData["content"] = decompressed
                    else:
                        pageData["content"] = pickle.loads(decompressed)
                    del pageData[target_field]
                    urlKeys[pageData["urlKey"]] = pageData
                else:
                    print("there was a problem processing URL %s" % pageData["url"])

            return {urlItem: urlKeys.get(db_normalize_url(urlItem), {"url": urlItem, "content": None, "error": True})
                    for urlItem in url_list}


