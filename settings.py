WEBCACHE_ADDRESS = "10.5.133.201:9011"
MONGO_LOCATION = "127.0.0.1"
FLASK_IP = "127.0.0.1"
# FLASK_IP = "10.5.133.201"

CACHE_ENVIRONMENT = {"dev": "localhost:9011", "docker": "webcache:9011"}

MAX_TIMES_FOR_URL = 20

FETCH_URL = "/fetch/<int:maxAgeDays>/<string:category>/<string:output>/<string:method>"
GET_PROXIES_URL = "/proxies/<int:numProxies>"
