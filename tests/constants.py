CREDIT_URL_TEMPLATE = "https://nominatim.openstreetmap.org/search/Credit Suisse,{},Switzerland?osm_type=N&format=json"
BANK_URL_TEMPLATE = "https://nominatim.openstreetmap.org/search/Bank,{},Switzerland?osm_type=N&format=json"
URLS_WITHOUT_PROXIES = ["http://localhost:7070/search/Switzerland?osm_type=N&format=json",
                        "http://localhost:7070/search/Germany?osm_type=N&format=json"]
POST_DATA_TEMPLATE = "https://search.wdoms.org"
POST_TEMPLATE = "https://search.wdoms.org?sSchoolName={}&iPageNumber=1"
MEDSCHOOLS = ['Basel', 'Zürich', 'London']
