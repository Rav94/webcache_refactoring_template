import json
import unittest

from tests.constants import CREDIT_URL_TEMPLATE, BANK_URL_TEMPLATE, URLS_WITHOUT_PROXIES, POST_TEMPLATE, \
    MEDSCHOOLS, POST_DATA_TEMPLATE
from webcache.webcacheclient import WebCacheClient


class TestClass(unittest.TestCase):
    def setUp(self):
        self.client = WebCacheClient()

    def test_get_request_credit(self):
        url_list = [CREDIT_URL_TEMPLATE.format(zipcode) for zipcode in range(2095, 2099)]
        ret = self.client.fetch_urls(url_list, category="OSM-geocoding", output="json")
        self.assertTrue(isinstance(ret, dict))
        self.assertFalse(any(obj.get('error') for obj in ret.values()))

    def test_get_request_bank(self):
        url_list = [BANK_URL_TEMPLATE.format(zipcode) for zipcode in range(2095, 2199)]
        ret = self.client.fetch_urls(url_list, category="OSM-geocoding", output="json")
        self.assertTrue(isinstance(ret, dict))
        self.assertFalse(any(obj.get('error') for obj in ret.values()))

    def test_get_request_without_proxies(self):
        ret = self.client.fetch_urls(URLS_WITHOUT_PROXIES, category="OSM-geocoding", output="json")
        self.assertTrue(isinstance(ret, dict))
        self.assertFalse(any(obj.get('error') for obj in ret.values()))

    def test_post_request(self):
        url_list = [POST_TEMPLATE.format(medschool) for medschool in MEDSCHOOLS]
        ret = self.client.fetch_urls(url_list, category="medschool", output="xml", method="POST")
        self.assertTrue(isinstance(ret, dict))
        self.assertFalse(any(obj.get('error') for obj in ret.values()))

    def test_post_data_request(self):
        url_list = [(POST_DATA_TEMPLATE, json.dumps({'sSchoolName': medschool, 'iPageNumber': 1}))
                    for medschool in MEDSCHOOLS]
        ret = self.client.fetch_urls(url_list, category="medschool", output="xml", method="POST")
        self.assertTrue(isinstance(ret, dict))
        self.assertFalse(any(obj.get('error') for obj in ret.values()))

    def test_get_proxy(self):
        proxy_list = self.client.get_proxy_list(10000)
        self.assertTrue(isinstance(proxy_list, list))
